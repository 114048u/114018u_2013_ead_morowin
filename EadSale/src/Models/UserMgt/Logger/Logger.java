package Models.UserMgt.Logger;


import java.util.ArrayList;

import javax.mail.Session;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class Logger {

	private String LoggerName;
	private String LoggerEmail;
	
	
	private String[] LoggerRoles;
	
	private ArrayList<Entity> LoggerFunctions;
	
	public String getLoggerName(){return LoggerName;}
	
	public void setLoggername(String name){
		
		LoggerName=name;
	}
	
	public String getLoggerEmail(){return LoggerEmail;}
	
	public void setLoggerEmail(String email){
		
		LoggerEmail=email;
	}
	
	
	public String[] getLoggerRoles(){
		
		return LoggerRoles;
	}
	
	public void setLoggerRoles(String[] roles){
		
		LoggerRoles=roles;
	}
	
	public ArrayList<Entity> getLoggerFunctions(){
		
		return LoggerFunctions;
	}

	public void setLoggerFunctions(ArrayList<Entity> functions){
	
	LoggerFunctions=functions;
}


	
	
	static DatastoreService datastore=DatastoreServiceFactory.getDatastoreService();
	
	public static boolean exists(String userId){
		
	
		Filter filter =
				                      new FilterPredicate("UserId",
				                      FilterOperator.EQUAL,
				                      userId);
		
		Query q = new Query("SysUser").setFilter(filter);
		
		PreparedQuery pq = datastore.prepare(q);

		Entity result = pq.asSingleEntity();
		
		if(result!=null){
			
			return true;
		}
		else{
			
			return false;
		}
		
		
		
		
	}
	
	public static Logger getLogger(String userId){
		
		Filter filter =
				  					  new FilterPredicate("UserId",
				                      FilterOperator.EQUAL,
				                      userId);
		
		
		Query q1 = new Query("SysUser").setFilter(filter);
		
		PreparedQuery pq1 = datastore.prepare(q1);

		Entity result = pq1.asSingleEntity();
		
		if((boolean)result.getProperty("Active")){
			
			Logger logger=new Logger();
			
			logger.LoggerEmail=(String) result.getProperty("Email");
			logger.LoggerName=(String)result.getProperty("FirstName")+" "+(String)result.getProperty("LastName");
			
			
			Query q2 = new Query("UserRole").setFilter(filter);
			
			PreparedQuery pq2 = datastore.prepare(q1);

			Entity result2 = pq2.asSingleEntity();
			
			String roles=(String) result2.getProperty("Roles");
			
			logger.LoggerRoles=roles.split("-1");
			
			
			String functionString="";
			
			for(int i=0;i<logger.LoggerRoles.length;i++){
				
				
				Filter filterf =
	  					  new FilterPredicate("RoleName",
	                      FilterOperator.EQUAL,
	                      logger.LoggerRoles[i]);


				Query q3 = new Query("RoleFunction").setFilter(filterf);

				PreparedQuery pq3 = datastore.prepare(q3);

				Entity result3 = pq3.asSingleEntity();
				
				
				functionString += (String)result3.getProperty("Functions");
				
								
			}
		
			String[] functionNames= functionString.split("-1");
			
			for(int j=0;j<functionNames.length;j++){
				
				
				Filter filterF =
	  					  new FilterPredicate("Function",
	                      FilterOperator.EQUAL,
	                      functionNames[j]);


				Query q4 = new Query("Function").setFilter(filterF);

				PreparedQuery pq4 = datastore.prepare(q4);

				Entity result4 = pq4.asSingleEntity();
				
				logger.LoggerFunctions.add(result4);
				
				
				
				}
			
			return logger;
			
			
		}else{
			return null;
			// still not authorized
			
		}
		
		
		
		
		
		
		
	}
	
}
