
<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Blog - Car Repair Shop Website Template</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie.css">
	<![endif]-->
</head>
<body>
<jsp:include page="Header.jsp" />
	
	<div id="body">
		<div class="content">
			<div class="section">
				<div class="blog">
					<div class="comment">
						<h4>one comment</h4>
						<div>
							<a href="#" class="figure"><img src="images/janedoe.jpg" alt="">jane doe</a>
							<p>
								<span>july 27, 2023</span> This is a comment. Nunc sed porta neque. Sed dui elit, condimentum venenatis pretium ut, pellentesque eget nulla. Nulla facilisi. <a href="#" class="reply">reply</a>
							</p>
						</div>
					</div>
					<div class="commentForm">
						<h4>leave a comment</h4>
						<form action="index.html">
							<div>
								<label for="name"> <span>name</span>
									<input type="text" name="name" id="name">
								</label>
								<label for="email"> <span>email</span>
									<input type="text" name="email" id="email">
								</label>
							</div>
							<label for="comment"> <span>comment</span>
								<textarea name="comment" id="comment"></textarea>
							</label>
							<input type="submit" name="Btncomment" id="Btncomment" value="">
						</form>
					</div>
				</div>
			</div>
			<div class="sidebar">

				<div class="post">
					<h3>popular blog posts</h3>
					<ul>
						<li>
							<a href="blog-single.html" class="figure"><img src="images/girl-calling.jpg" alt=""></a>
							<div>
								<span><a href="blog-single.html">Head</a></span>
								<p>
									Information technology
									Information technology
								</p>
							</div>
						</li>
						<li>
							<a href="blog-single.html" class="figure"><img src="images/man2.jpg" alt=""></a>
							<div>
								<span><a href="blog-single.html">head</a></span>
								<p>
									Information technology								</p>
							</div>
						</li>
						<li>
							<a href="blog-single.html" class="figure"><img src="images/gentleman.jpg" alt=""></a>
							<div>
								<span><a href="blog-single.html">head</a></span>
								<p>
									Information technology								</p>
							</div>
						</li>
						<li>
							<a href="blog-single.html" class="figure"><img src="images/castle2.jpg" alt=""></a>
							<div>
								<span><a href="blog-single.html">head</a></span>
								<p>
									Information technology								</p>
							</div>
						</li>
					</ul>
				</div>
		</div>
	</div>
	
	<div id="footer">
		<div>
			<div class="contact">
				<h3>contact information</h3>
				<ul>
					<li>
						<b>address:</b> <span>426 Grant Street colombo 10</span>
					</li>
					<li>
						<b>phone:</b> <span>0112222555</span>
					</li>
					<li>
						<b>fax:</b> <span>0112222555</span>
					</li>
					<li>
						<b>email:</b> <span><a href="http://www.bfc.com/misc/con">bfc@hotmail.com</a></span>
					</li>
				</ul>
			</div>
			<div class="tweets">
				<h3>recent tweets</h3>
				<ul>
					<li>
						<a href="#">Information technology Information technology<span>1 day ago</span></a>
					</li>
					<li>
						<a href="#">Information technology Information technology<span>2 days ago</span></a>
					</li>
				</ul>
			</div>
			<div class="posts">
				<h3>recent blog post</h3>
				<ul>
					<li>
						<a href="#">Information technology </a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology Information technology </a>
					</li>
				</ul>
			</div>
			<div class="connect">
				<h3>stay in touch</h3>
				<p>
					Information technology Information technology Information technology Information technology				</p>
				<ul>
					<li id="facebook">
						<a href="https://facebook.com">facebook</a>
					</li>
					<li id="twitter">
						<a href="https://twitter.com">twitter</a>
					</li>
					<li id="googleplus">
						<a href="https://google.com">googleplus</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="section">
			<p>
				&copy; BY MOROWING
			</p>
			<ul>
				<li>
					<a href="index.html">home</a>
				</li>
				<li>
					<a href="about.html">about</a>
				</li>
				<li>
					<a href="services.html">services</a>
				</li>
				<li>
					<a href="blog.html">blog</a>
				</li>
				<li>
					<a href="contact.html">contact</a>
				</li>
				<li>
					<a href="booking.html">book an appointment</a>
				</li>
			</ul>
		</div>
	</div>
</div>
</body>
</html>