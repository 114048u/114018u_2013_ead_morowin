<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Contact - Car Repair Shop Website Template</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie.css">
	<![endif]-->
</head>
<body>
<jsp:include page="Header.jsp" />
	
	<div id="body">
		<div class="content">
			<div class="section">
				<div class="contact">
					<h2>get in touch with us!</h2>
					<form action="index.html">
						<label for="name"> <span>name</span>
							<input type="text" name="name" id="name">
						</label>
						<label for="email"> <span>email</span>
							<input type="text" name="email" id="email">
						</label>
						<label for="subject"> <span>subject</span>
							<input type="text" name="subject" id="subject">
						</label>
						<label for="message"> <span>comment</span>
							<textarea name="message" id="message"></textarea>
						</label>
						<input type="submit" name="send" id="send" value="">
					</form>
				</div>
			</div>
			<div class="sidebar">
				<div class="contact">
					<div>
						<a href="gallery.html"><img src="images/lava2.jpg" alt=""></a>
					</div>
					<h4>contact information</h4>
					<ul>
						<li>
							<b>address:</b> <span>426 Grant Street colombo 10</span>
						</li>
						<li>
							<b>phone:</b> <span>0112222555</span>
						</li>
						<li>
							<b>fax:</b> <span>0112222555</span>
						</li>
						<li>
							<b>email:</b> <span><a href="http://www.bfc.com/misc/contact">bfc@hotmail.com</a></span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div>
			<div class="contact">
				<h3>contact information</h3>
				<ul>
					<li>
						<b>address:</b> <span>426 Grant Street colombo 10</span>
					</li>
					<li>
						<b>phone:</b> <span>0112222555</span>
					</li>
					<li>
						<b>fax:</b> <span>0112222555</span>
					</li>
					<li>
						<b>email:</b> <span><a href="http://www.bfc.com/misc/con">bfc@hotmail.com</a></span>
					</li>
				</ul>
			</div>
			<div class="tweets">
				<h3>recent tweets</h3>
				<ul>
					<li>
						<a href="#">Information technology Information technology<span>1 day ago</span></a>
					</li>
					<li>
						<a href="#">Information technology Information technology<span>2 days ago</span></a>
					</li>
				</ul>
			</div>
			<div class="posts">
				<h3>recent blog post</h3>
				<ul>
					<li>
						<a href="#">Information technology </a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology Information technology </a>
					</li>
				</ul>
			</div>
			<div class="connect">
				<h3>stay in touch</h3>
				<p>
					Information technology Information technology Information technology Information technology				</p>
				<ul>
					<li id="facebook">
						<a href="https://facebook.com">facebook</a>
					</li>
					<li id="twitter">
						<a href="https://twitter.com">twitter</a>
					</li>
					<li id="googleplus">
						<a href="https://google.com">googleplus</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="section">
			<p>
				&copy; BY MOROWING
			</p>
			<ul>
				<li>
					<a href="index.html">home</a>
				</li>
				<li>
					<a href="about.html">about</a>
				</li>
				<li>
					<a href="services.html">services</a>
				</li>
				<li>
					<a href="blog.html">blog</a>
				</li>
				<li>
					<a href="contact.html">contact</a>
				</li>
				<li>
					<a href="booking.html">book an appointment</a>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>